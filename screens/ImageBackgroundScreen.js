import React, { Component } from 'react';
import { StyleSheet, Text, View, Button , AppRegistry, Image , ImageBackground } from 'react-native';

class ImageBackgroundScreen extends Component {
    render() {
        return (
            <ImageBackground source={{uri: 'https://facebook.github.io/react-native/docs/assets/favicon.png'}} style={{width: '100%', height: '100%'}}>
              <Text>Inside</Text>
            </ImageBackground>
          );
    }
}



export default ImageBackgroundScreen;
