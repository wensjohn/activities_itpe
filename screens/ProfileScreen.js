import React, {Component} from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';

class ProfileScreen extends Component {
    render() {
        return (
          <View style={styles.container}>
           
            <Text>Janel Jimenez</Text>
            <Text>28 years old</Text>
            <Text>wensjohn19@gmail.com</Text>
            <Text>University of Southeastern Philippines</Text>
            <Text>BS of Information Technology</Text>
            <Text>BSIT-3B / 3rd year</Text>
            <Text>ICLC Governor 2018</Text>
            <Button
              title="Home"
              onPress={() => this.props.navigation.navigate('Home')}
            />
          </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: 'yellow',
      alignItems: 'center',
      justifyContent: 'center',
    },
  });

const fontSize = StyleSheet.create({
    container: {
      fontSize: 20, 
    },
  });


export default ProfileScreen;
