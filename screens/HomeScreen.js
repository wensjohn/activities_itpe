import React, { Component } from 'react';
import { StyleSheet, Text, View, Button , ActivityIndicator,
  AppRegistry, Image , ScrollView } from 'react-native';

class HomeScreen extends Component {
    render() {
        return (
          <View style={styles.container}>
            <Text> Welcome to My Profile</Text>
            <ScrollView>
            <Button
              title="Details"
              onPress={() => this.props.navigation.navigate('Details')}
            />
             <Button
              title="ActivityIndicator"
              onPress={() => this.props.navigation.navigate('ActivityIndicator')}
            />
            <Button
              title="DrawerLayoutAndroid"
              onPress={() => this.props.navigation.navigate('DrawerLayoutAndroid')}
            />
            <Button
              title="FlatList"
              onPress={() => this.props.navigation.navigate('FlatList')}
            />
            <Button
              title="Image"
              onPress={() => this.props.navigation.navigate('Image')}
            />
            <Button
              title="ImageBackground"
              onPress={() => this.props.navigation.navigate('ImageBackground')}
            />
            <Button
              title="KeyboardAvoidingView"
              onPress={() => this.props.navigation.navigate('KeyboardAvoidingView')}
            />
            <Button
              title="ListView"
              onPress={() => this.props.navigation.navigate('ListView')}
            />
            <Button
              title="Modal"
              onPress={() => this.props.navigation.navigate('Modal')}
            />
            <Button
              title="Picker"
              onPress={() => this.props.navigation.navigate('Picker')}
            />
            <Button
              title="ProgressBarAndroid"
              onPress={() => this.props.navigation.navigate('ProgressBarAndroid')}
            />
             <Button
              title="RefreshControl"
              onPress={() => this.props.navigation.navigate('RefreshControl')}
            />
            <Button
              title="ScrollView"
              onPress={() => this.props.navigation.navigate('ScrollView')}
            />
            <Button
              title="SectionList"
              onPress={() => this.props.navigation.navigate('SectionList')}
            />
            <Button
              title="Slider"
              onPress={() => this.props.navigation.navigate('Slider')}
            />
            <Button
              title="StatusBar"
              onPress={() => this.props.navigation.navigate('StatusBar')}
            />
            <Button
              title="Switch"
              onPress={() => this.props.navigation.navigate('Switch')}
            />
            <Button
              title="TextInput"
              onPress={() => this.props.navigation.navigate('TextInput')}
            />
             <Button
              title="Text"
              onPress={() => this.props.navigation.navigate('Text')}
            />
             <Button
              title="TouchableHighlight"
              onPress={() => this.props.navigation.navigate('TouchableHighlight')}
            />
            <Button
              title="TouchableNativeFeedback"
              onPress={() => this.props.navigation.navigate('TouchableNativeFeedback')}
            />
            <Button
              title="TouchableOpacity"
              onPress={() => this.props.navigation.navigate('TouchableOpacity')}
            />
            <Button
              title="ViewPager"
              onPress={() => this.props.navigation.navigate('ViewPager')}
            />
            <Button
              title="View"
              onPress={() => this.props.navigation.navigate('View')}
            />
             <Button
              title="WebView"
              onPress={() => this.props.navigation.navigate('WebView')}
            />
            </ScrollView>
          </View>

        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: 'yellow',
      alignItems: 'center',
      justifyContent: 'center',

    },
  });

export default HomeScreen;
