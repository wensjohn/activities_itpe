import React from 'react';
import { StyleSheet, Text, View, Button , AppRegistry, Image  } from 'react-native';
import { StackNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation';
import { createDrawerNavigator } from 'react-navigation';
import HomeScreen from './screens/HomeScreen'
import DetailsScreen from './screens/DetailsScreen'
import ProfileScreen from './screens/ProfileScreen'
import ActivityIndicatorScreen from './screens/ActivityIndicatorScreen'
import DrawerLayoutAndroidScreen from './screens/DrawerLayoutAndroidScreen'
import FlatListScreen from './screens/FlatListScreen'
import ImageScreen from './screens/ImageScreen'
import ImageBackgroundScreen from './screens/ImageBackgroundScreen'
import KeyboardAvoidingViewScreen from './screens/KeyboardAvoidingViewScreen'
import ListViewScreen from './screens/ListViewScreen'
import ModalScreen from './screens/ModalScreen'
import PickerScreen from './screens/PickerScreen'
import ProgressBarAndroidScreen from './screens/ProgressBarAndroidScreen'
import RefreshControlScreen from './screens/RefreshControlScreen'
import ScrollViewScreen from './screens/ScrollViewScreen'
import SectionListScreen from './screens/SectionListScreen'
import SliderScreen from './screens/SliderScreen'
import StatusBarScreen from './screens/StatusBarScreen'
import SwitchScreen from './screens/SwitchScreen'
import TextInputScreen from './screens/TextInputScreen'
import TextScreen from './screens/TextScreen'
import TouchableHighlightScreen from './screens/TouchableHighlightScreen'
import TouchableNativeFeedbackScreen from './screens/TouchableNativeFeedbackScreen'
import TouchableOpacityScreen from './screens/TouchableOpacityScreen'
import ViewPagerScreen from './screens/ViewPagerScreen'
import ViewScreen from './screens/ViewScreen'
import WebViewScreen from './screens/WebViewScreen'



const RootStack = StackNavigator (
  {
    Home: {
      screen: HomeScreen,
    },
    Details: {
      screen: DetailsScreen,
    },
    Profile: {
      screen: ProfileScreen,
    },
     ActivityIndicator: {
      screen: ActivityIndicatorScreen,
    },
    DrawerLayoutAndroid: {
      screen: DrawerLayoutAndroidScreen,
    },
    FlatList: {
      screen: FlatListScreen,
    },
    Image: {
      screen: ImageScreen,
    },
    ImageBackground: {
      screen: ImageBackgroundScreen,
    },
    KeyboardAvoidingView: {
      screen: KeyboardAvoidingViewScreen,
    },
    ListView: {
      screen: ListViewScreen,
    },
    Modal: {
      screen: ModalScreen,
    },
    Picker: {
      screen: PickerScreen,
    },
    ProgressBarAndroid: {
      screen: ProgressBarAndroidScreen,
    },
    RefreshControl: {
      screen: RefreshControlScreen,
    },
    ScrollView: {
      screen: ScrollViewScreen,
    },
    SectionList: {
      screen: SectionListScreen,
    },
    Slider: {
      screen: SliderScreen,
    },
    StatusBar: {
      screen: StatusBarScreen,
    },
    Switch: {
      screen: SwitchScreen,
    },
    Switch: {
      screen: SwitchScreen,
    },
    TextInput: {
      screen: TextInputScreen,
    },
    Text: {
      screen: TextScreen,
    },
    TouchableHighlight: {
      screen: TouchableHighlightScreen,
    },
    TouchableNativeFeedback: {
      screen: TouchableNativeFeedbackScreen,
    },
    TouchableOpacity: {
      screen: TouchableOpacityScreen,
    },
    ViewPager: {
      screen: ViewPagerScreen,
    },
    View: {
      screen: ViewScreen,
    },
    WebView: {
      screen: WebViewScreen,
    },
  },
  {
    initialRouteName: 'Home',
  }
);

export default class App extends React.Component {
  render() {
    return <RootStack />;
  }
}
